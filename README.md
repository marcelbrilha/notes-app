# Notes APP

# Run

## Install dependencies
npm install

## Linter
npm run lint

## Help commands
node app.js --help

## List notes
node app.js list

## Find a note
node app.js find --title="Note title"

## Add a new note
node app.js add --title="Note title" --body="Note body"

## Remove a note
node app.js remove --title="Note title"
