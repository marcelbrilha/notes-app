const yargs = require('yargs')
const chalk = require('chalk')

const notes = require('./notes')
const { version } = require('./package.json')

yargs.version(version)

yargs.command({
  command: 'list',
  describe: 'List notes',
  handler () {
    const allNotes = notes.getAll()
    console.log(allNotes)
  }
})

yargs.command({
  command: 'find',
  describe: 'Find a note',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true,
      type: 'string'
    }
  },
  handler ({ title }) {
    const note = notes.findNote(title)
    note
      ? console.log(note)
      : console.log(chalk.red.inverse('No note found!'))
  }
})

yargs.command({
  command: 'add',
  describe: 'Add a new note',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true,
      type: 'string'
    },
    body: {
      describe: 'Note body',
      demandOption: true,
      type: 'string'
    }
  },
  handler ({ title, body }) {
    notes.addNote({ title, body })
  }
})

yargs.command({
  command: 'remove',
  describe: 'Remove a note',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true,
      type: 'string'
    }
  },
  handler ({ title }) {
    notes.removeNote(title)
  }
})
