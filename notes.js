const fs = require('fs')
const chalk = require('chalk')

const getAll = () => {
  return _loadNotes()
}

const findNote = (title) => {
  const notes = _loadNotes()
  const note = notes.find(
    note => note.title.toLowerCase() === title.toLowerCase()
  )

  if (note) {
    return note
  } else {
    console.log(chalk.red.inverse('No note found!'))
    return false
  }
}

const addNote = (note) => {
  const notes = _loadNotes()
  const newNotes = [
    ...notes,
    note
  ]

  _saveNote(newNotes)
  console.log(chalk.green.inverse('Note added!'))
}

const removeNote = (title) => {
  const notes = _loadNotes()
  const noteExists = findNote(title)

  if (noteExists) {
    const newNotes = notes.filter(
      note => note.title.toLowerCase() !== title.toLowerCase()
    )
    _saveNote(newNotes)
    console.log(chalk.green.inverse('Note removed!'))
  }
}

const _saveNote = (newNotes) => {
  const dataJSON = JSON.stringify(newNotes)
  fs.writeFileSync('notes.json', dataJSON)
}

const _loadNotes = () => {
  try {
    const dataBuffer = fs.readFileSync('notes.json')
    const dataJson = dataBuffer.toString()
    return JSON.parse(dataJson)
  } catch (exception) {
    console.log(exception)
    return []
  }
}

module.exports = {
  getAll,
  findNote,
  addNote,
  removeNote
}
